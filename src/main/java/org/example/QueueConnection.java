package org.example;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.example.exeptions.ActiveMQConnectException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jms.Connection;
import javax.jms.JMSException;
import javax.jms.Session;

import static javax.jms.Session.AUTO_ACKNOWLEDGE;

public class QueueConnection {
    private Connection connection;
    private final ActiveMQConnectionFactory connectionFactory;
    public static final Logger LOGGER = LoggerFactory.getLogger(QueueConnection.class);

    public QueueConnection(String username, String password, String url) {
        this.connectionFactory = new ActiveMQConnectionFactory(username, password, url);
    }

    public Session getSession() {
        try {
            if (connection != null) {
                LOGGER.info("Creating new session");
                return connection.createSession(false, AUTO_ACKNOWLEDGE);
            }
            LOGGER.info("Connection to ActiveMQ created successfully");
            LOGGER.info("Creating new session");
            connection = connectionFactory.createConnection();
            connection.start();
            return connection.createSession(false, AUTO_ACKNOWLEDGE);
        } catch (JMSException e) {
            LOGGER.error("Cannot connect or create session", e);
            throw new ActiveMQConnectException(e);
        }
    }

    public void closeConnection() {
        try {
            connection.close();
        } catch (JMSException e) {
            LOGGER.error("Failed to close connections of the queue", e);
            throw new ActiveMQConnectException(e);
        }
    }
}
