package org.example;

import org.example.exeptions.NoSuchPropertyException;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import static org.example.App.LOGGER;

public class PropertiesLoader {
    private static final String CONFIG_FILENAME = "config.properties";

    public Properties loadProperties() {
        File file = new File("/opt/test/",CONFIG_FILENAME);
        Properties properties = new Properties();
        try {
            // Use the current class's ClassLoader to access resources
            ClassLoader classLoader = App.class.getClassLoader();
            InputStream input = classLoader.getResourceAsStream(CONFIG_FILENAME);

            if (input != null) {
                properties.load(input);
            } else {
                LOGGER.error("File not found in classpath: " + CONFIG_FILENAME);
                System.exit(1);
            }
        } catch (IOException e) {
            LOGGER.error("Error occurred while loading properties",e);
        }
        checkLoadedProperties(properties);
        return properties;
    }

    private void checkLoadedProperties(Properties properties) {
        String[] requiredProperties = {"login", "password", "url", "time", "queueName"};

        for (String property : requiredProperties) {
            if (!properties.containsKey(property)) {
                LOGGER.error("Property \"{}\" NOT present in properties file",property);
                throw new NoSuchPropertyException();
            }
        }
    }
}
