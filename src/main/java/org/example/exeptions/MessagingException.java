package org.example.exeptions;

public class MessagingException extends RuntimeException {
    public MessagingException(Throwable cause) {
        super(cause);
    }

    public MessagingException(String message, Throwable cause) {
        super(message, cause);
    }
}
