package org.example.exeptions;

public class ActiveMQConnectException extends RuntimeException{
    public ActiveMQConnectException(Throwable cause) {
        super(cause);
    }
    public ActiveMQConnectException(String message, Throwable cause) {
        super(message, cause);
    }
}
