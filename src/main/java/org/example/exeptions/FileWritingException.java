package org.example.exeptions;

public class FileWritingException extends RuntimeException {
    public FileWritingException(Throwable cause) {
        super(cause);
    }
}
