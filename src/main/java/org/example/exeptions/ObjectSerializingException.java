package org.example.exeptions;

public class ObjectSerializingException extends RuntimeException {
    public ObjectSerializingException(Throwable cause) {
        super(cause);
    }
}
