package org.example;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.example.exeptions.FileWritingException;
import org.example.exeptions.MessagingException;
import org.example.user.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.Session;
import javax.jms.TextMessage;
import java.io.IOException;

import static org.example.App.MILLIS_IN_SECOND;
import static org.example.Producer.POISON_PILL_MESSAGE;

public class Consumer {
    private final ObjectMapper mapper;
    private final Session session;
    private final MessageConsumer messageConsumer;
    private final MessageWriter messageWriter;
    private static final Logger LOGGER = LoggerFactory.getLogger(Consumer.class);

    public Consumer(Session session, MessageWriter messageWriter,String queueName) {
        try {
            this.session = session;
            mapper = new ObjectMapper();
            messageConsumer = session.createConsumer(session.createQueue(queueName));
            this.messageWriter = messageWriter;
            LOGGER.debug("Consumer successfully created");
        } catch (JMSException e) {
            LOGGER.error("Error occurred while creating Consumer", e);
            throw new MessagingException(e);
        }
    }

    public void receiveMessages() {
        int counter = 0;
        String message = "";
        long start = System.currentTimeMillis();
        LOGGER.warn("START OF RECEIVING MESSAGES");
        while (true) {
            try {
                message = ((TextMessage) messageConsumer.receive()).getText();
            } catch (JMSException e) {
                LOGGER.error("Error occurred while reading messages from queue", e);
                throw new MessagingException(e);
            }
            if (message.equals(POISON_PILL_MESSAGE)) {
                LOGGER.warn("POISON PILL CAUGHT, STOP RECEIVING MESSAGES");
                break;
            }
            processAndWriteMessage(message, messageWriter);
            counter++;
        }
        double time = (double) (System.currentTimeMillis() - start) / MILLIS_IN_SECOND;
        LOGGER.info("Total messages received: {}, time: {} s, RPS: {}", counter, time, counter / time);
    }

    public void processAndWriteMessage(String message, MessageWriter messageWriter) {
        User mappedMessage;
        try {
            mappedMessage = mapper.readValue(message, User.class);
        } catch (JsonProcessingException e) {
            LOGGER.warn("Received incorrect message!");
            messageWriter.writeErrorMessage(message);
            return;
        }
        if (messageWriter.messageIsCorrect(mappedMessage)) {
            messageWriter.writeCorrectMessage(mappedMessage);
        } else {
            messageWriter.writeIncorrectMessage(mappedMessage);
        }
    }

    public void closeConsumerConnections() {
        try {
            session.close();
            messageConsumer.close();
            messageWriter.close();
        } catch (JMSException e) {
            LOGGER.error("Can't close consumer connections", e);
            throw new MessagingException(e);
        } catch (IOException e) {
            LOGGER.error("IO exception occurred while processing messages from the queue");
            throw new FileWritingException(e);
        }
    }
}
