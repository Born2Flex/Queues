package org.example;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.example.exeptions.MessagingException;
import org.example.exeptions.ObjectSerializingException;
import org.example.user.User;
import org.example.user.UserGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jms.DeliveryMode;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Session;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;

import static org.example.App.MILLIS_IN_SECOND;

public class Producer {
    private final long messages;
    private final long timeToSend;
    private final ObjectMapper mapper;
    private final UserGenerator userGenerator;
    private final Session session;
    private final MessageProducer messageProducer;
    private static final Logger LOGGER = LoggerFactory.getLogger(Producer.class);
    public static final String POISON_PILL_MESSAGE = "POISON PILL: STOP READING";

    public Producer(Session session, String queueName, long messageLimit, long timeToSend) {
        try {
            this.session = session;
            this.messages = messageLimit;
            this.timeToSend = timeToSend * MILLIS_IN_SECOND;
            mapper = new ObjectMapper();
            userGenerator = new UserGenerator();
            messageProducer = session.createProducer(session.createQueue(queueName));
            messageProducer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
            LOGGER.debug("Producer successfully created");
        } catch (JMSException e) {
            LOGGER.error("Failed to create producer", e);
            throw new MessagingException(e);
        }
    }

    public void sendMessages() {
        final AtomicInteger counter = new AtomicInteger();
        long start = System.currentTimeMillis();
        LOGGER.warn("START OF SENDING MESSAGES");
        Stream.generate(userGenerator::generateUser)
                .limit(messages)
                .takeWhile(message -> System.currentTimeMillis() - start < timeToSend)
                .forEach(message -> {
                    sendMessageToQueue(serializeUser(message));
                    counter.getAndIncrement();
                });
//        User user = new User();
//        user.setEddr("2005051601671");
//        user.setName("Danylo");
//        user.setCount(55);
//        user.setCreationTime(LocalDate.now().toString());
//        sendMessageToQueue(serializeUser(user));
        LOGGER.debug("Sending POISON PILL to the queue");
        sendMessageToQueue(POISON_PILL_MESSAGE);
        double time = (double) (System.currentTimeMillis() - start) / MILLIS_IN_SECOND;
        LOGGER.warn("SENDING MESSAGES STOPPED");
        LOGGER.info("Total messages sent: {}, time: {} s, RPS: {}", counter.get(), time, counter.get() / time);
    }

    private void sendMessageToQueue(String message) {
        try {
            messageProducer.send(session.createTextMessage(message));
        } catch (JMSException e) {
            LOGGER.error("Error occurred while sending message to queue", e);
            throw new MessagingException(e);
        }
    }

    private String serializeUser(User user) {
        try {
            return mapper.writeValueAsString(user);
        } catch (JsonProcessingException e) {
            LOGGER.error("Cannot serialize object to String", e);
            throw new ObjectSerializingException(e);
        }
    }

    public void closeProducerConnections() {
        try {
            session.close();
            messageProducer.close();
        } catch (JMSException e) {
            LOGGER.error("Can't close producer connection", e);
            throw new MessagingException(e);
        }
    }
}
