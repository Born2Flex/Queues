package org.example;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Properties;

public class App {
    public static final Logger LOGGER = LoggerFactory.getLogger(App.class);
    public static final int MILLIS_IN_SECOND = 1000;
    public static final int MIN_MESSAGES = 1001;

    public static void main(String[] args) throws IOException {

        LOGGER.info("Start of program");
        PropertiesLoader propertiesLoader = new PropertiesLoader();
        Properties properties = propertiesLoader.loadProperties();
        App app = new App();
        int messagesNumber = app.getMessageNumber();

        QueueConnection connection = new QueueConnection(properties.getProperty("login"),
                properties.getProperty("password"),properties.getProperty("url"));
        Producer producer = new Producer(connection.getSession(),properties.getProperty("queueName"),
                messagesNumber, Integer.parseInt(properties.getProperty("time")));

//        QueueConnection connection = new QueueConnection("admin",
//                "admin", "tcp://localhost:61616");
//        Producer producer = new Producer(connection.getSession(), "MyQueue",
//                1_000_000, 180);

        producer.sendMessages();

        Consumer consumer = new Consumer(connection.getSession(), new MessageWriter(), "MyQueue");
        consumer.receiveMessages();
        connection.closeConnection();
        producer.closeProducerConnections();
        consumer.closeConsumerConnections();
    }

    private int getMessageNumber() {
        String messages = System.getProperty("N");
        if (messages == null) {
            LOGGER.debug("System property N not given, using DEFAULT VALUE (N = {})", MIN_MESSAGES);
            return MIN_MESSAGES;
        } else {
            int N = Integer.parseInt(messages);
            if (Integer.parseInt(messages) < MIN_MESSAGES) {
                LOGGER.debug("Given N property is too small (N < {}), using DEFAULT VALUE", MIN_MESSAGES);
                return MIN_MESSAGES;
            } else {
                LOGGER.debug("Using N property with value {}", N);
                return N;
            }
        }
    }
}
