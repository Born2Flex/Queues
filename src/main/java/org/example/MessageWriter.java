package org.example;

import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validation;
import jakarta.validation.Validator;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.example.exeptions.FileWritingException;
import org.example.user.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Closeable;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Set;
import java.util.stream.Collectors;

public class MessageWriter implements Closeable {
    private final CSVPrinter correctCsvWriter;
    private final CSVPrinter errorsCsvWriter;
    private final FileWriter incorrectWriter;
    private final Validator validator;
    private static final String CORRECT_MESSAGES = "correct.csv";
    private static final String INCORRECT_MESSAGES = "incorrect.csv";
    private static final String ERROR_MESSAGES = "errors.csv";
    private static final Logger LOGGER = LoggerFactory.getLogger(MessageWriter.class);

    public MessageWriter() {
        try {
            correctCsvWriter = new CSVPrinter(new FileWriter(CORRECT_MESSAGES),
                    CSVFormat.DEFAULT.builder().setHeader("name", "count").build());
            errorsCsvWriter = new CSVPrinter(new FileWriter(INCORRECT_MESSAGES),
                    CSVFormat.DEFAULT.builder().setHeader("name", "count", "errors").build());
            incorrectWriter = new FileWriter(ERROR_MESSAGES);
            validator = Validation.buildDefaultValidatorFactory().getValidator();
        } catch (IOException e) {
            LOGGER.error("Failed to create MessageWriter object", e);
            throw new FileWritingException(e);
        }
    }

    public void writeCorrectMessage(User message) {
        try {
            correctCsvWriter.printRecord(message.getName(), message.getCount());
        } catch (IOException e) {
            LOGGER.error("Failed to write correct message in file");
            throw new FileWritingException(e);
        }
    }

    public void writeIncorrectMessage(User message) {
        Set<ConstraintViolation<User>> errors = validator.validate(message);
        String formattedErrors = formatErrors(errors);
        try {
            errorsCsvWriter.printRecord(message.getName(), message.getCount(), formattedErrors);
        } catch (IOException e) {
            LOGGER.error("Failed to write incorrect message in file");
            throw new FileWritingException(e);
        }
    }

    public void writeErrorMessage(String message) {
        try {
            incorrectWriter.write(message + "\n");
        } catch (IOException e) {
            LOGGER.error("Failed to write correct message in file");
            throw new FileWritingException(e);
        }
    }

    private String formatErrors(Set<ConstraintViolation<User>> errors) {

        return errors.stream()
                .map(ConstraintViolation::getMessage)
                .sorted()
                .collect(Collectors.joining(", ", "{errors: [", "]}"));
    }

    public boolean messageIsCorrect(User message) {
        Set<ConstraintViolation<User>> errors = validator.validate(message);
        return errors.isEmpty();
    }

    @Override
    public void close() throws IOException {
        correctCsvWriter.close();
        errorsCsvWriter.close();
        incorrectWriter.close();
    }

}
