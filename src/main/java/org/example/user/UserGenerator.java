package org.example.user;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Random;

public class UserGenerator {
    public static final DateTimeFormatter DATE_TIME_FORMAT = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static final String CHARACTERS = "abcdefghijklmnopqrstuvwxyz";
    private static final Random RANDOM = new Random();
    private static final int EDDR_LENGTH = 13;
    private static final int NAME_MIN_LENGTH = 5;
    private static final int NAME_MAX_LENGTH = 10;
    private static final int MIN_YEAR = 1900;
    private static final int MAX_YEAR = 2023;
    private static final int MAX_MONTH = 13;
    private static final int MIN_MONTH = 1;
    private static final int MAX_DAY = 29;
    private static final int MIN_DAY = 1;
    private static final int DATE_LENGTH = 8;
    private static final int MAX_COUNT = 100;

    public User generateUser() {
        User user = new User();
        user.setName(generateName());
        user.setCount(generateCount());
        user.setEddr(generateEDDR());
        user.setCreationTime(LocalDateTime.now().toString());
        return user;
    }

    private String generateName() {
        int length = getRandomInt(NAME_MIN_LENGTH, NAME_MAX_LENGTH);
        StringBuilder builder = new StringBuilder();
        int k = 0;
        while (k < length) {
            builder.append(CHARACTERS.charAt(RANDOM.nextInt(CHARACTERS.length())));
            k++;
        }
        return builder.toString();
    }

    private int generateCount() {
        return RANDOM.nextInt(MAX_COUNT);
    }

    private String generateEDDR() {
        StringBuilder builder = new StringBuilder();
        builder.append(generateDate().format(DATE_TIME_FORMAT));
        int k = 0;
        while (k < EDDR_LENGTH - DATE_LENGTH) {
            builder.append(RANDOM.nextInt(10));
            k++;
        }
        return builder.toString();
    }

    private LocalDate generateDate() {
        return LocalDate.of(getRandomInt(MIN_YEAR, MAX_YEAR), getRandomInt(MIN_MONTH, MAX_MONTH), getRandomInt(MIN_DAY, MAX_DAY));
    }

    private int getRandomInt(int lowerBound, int upperBound) {
        return RANDOM.nextInt(upperBound - lowerBound) + lowerBound;
    }
}
