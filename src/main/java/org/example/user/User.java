package org.example.user;

import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.Pattern;
import org.example.eddr.EDDRValid;
import org.hibernate.validator.constraints.Length;

public class User {
    @Pattern(regexp = "\\w*a\\w*", message = "Name must contain 'a' letter")
    @Length(min = 6, message = "Name is too short")
    private String name;
    @EDDRValid()
    private String eddr;
    @Min(value = 10, message = "Count must be more or equal to 10")
    private int count;
    private String creationTime;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEddr() {
        return eddr;
    }

    public void setEddr(String eddr) {
        this.eddr = eddr;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(String creationTime) {
        this.creationTime = creationTime;
    }
}
