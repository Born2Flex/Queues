package org.example.eddr;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target(FIELD)
@Retention(RUNTIME)
@Documented
@Constraint(validatedBy = EDDRValidator.class)
public @interface EDDRValid {
    String message() default "Incorrect EDDR code";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
