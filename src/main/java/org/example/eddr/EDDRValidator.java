package org.example.eddr;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.regex.Pattern;

import static org.example.user.UserGenerator.DATE_TIME_FORMAT;

public class EDDRValidator implements ConstraintValidator<EDDRValid, String> {
    private static final int[] WEIGHT_COEFFICIENTS = {7, 3, 1};
    public static final Pattern PATTERN_SEPARATED = Pattern.compile("\\d{8}-\\d{5}");
    public static final Pattern PATTERN_REGULAR = Pattern.compile("\\d{13}");

    @Override
    public boolean isValid(String code, ConstraintValidatorContext constraintValidatorContext) {
        if (!PATTERN_SEPARATED.matcher(code).matches()
                && !PATTERN_REGULAR.matcher(code).matches()) {
            return false;
        }

        return checkDate(code) && controlNumberIsValid(code);
    }

    private boolean checkDate(String code) {
        try {
            String date = code.substring(0, 8);
            LocalDate.parse(date, DATE_TIME_FORMAT);
        } catch (DateTimeParseException e) {
            return false;
        }
        return true;
    }

    private boolean controlNumberIsValid(String code) {
        int currPointer = 0;
        int controlSum = 0;

        for (int i = 0; i < code.length() - 1; i++) {
            if (code.charAt(i) == '-')
                continue;
            controlSum += (code.charAt(i) - '0') * WEIGHT_COEFFICIENTS[currPointer % 3];
            currPointer++;
        }

        return controlSum % 10 == code.charAt(code.length() - 1) - '0';
    }
}
