package org.example;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;
import java.time.Duration;

import static org.example.App.MILLIS_IN_SECOND;
import static org.junit.jupiter.api.Assertions.assertTimeout;
import static org.mockito.Mockito.*;

class ProducerTest {
    public static final int DELAY = 30;
    private Producer producer;
    private Session session;
    private MessageProducer messageProducer;

    @BeforeEach
    void setup() throws JMSException {
        messageProducer = mock(MessageProducer.class);
        session = mock(Session.class);
        Queue destination = mock(Queue.class);

        when(session.createQueue("test")).thenReturn(destination);
        when(session.createProducer(destination)).thenReturn(messageProducer);
    }

    @ParameterizedTest
    @CsvSource(value = {"1680","452","4353","0"})
    void sendMessagesTest_ShouldSendCorrectNumOfMessages(int num) throws JMSException {
        producer = new Producer(session, "test", num, 10);
        doNothing().when(messageProducer).send(null);

        producer.sendMessages();

        verify(messageProducer, times(num + 1)).send(null);
    }

    @ParameterizedTest
    @CsvSource(value = {"0","1","3"})
    void sendMessagesTest_ShouldStopSendingByTime(int time) throws JMSException {
        producer = new Producer(session, "test", 1_000_000_000, time);
        doNothing().when(messageProducer).send(null);

        assertTimeout(Duration.ofMillis(time * MILLIS_IN_SECOND + DELAY), () -> producer.sendMessages());
    }
    @Test
    void sendMessagesTest_WhenTTSIsZero() throws JMSException {
        producer = new Producer(session, "test", 160, 0);
        doNothing().when(messageProducer).send(null);

        producer.sendMessages();

        verify(messageProducer, times(1)).send(null);
    }

}