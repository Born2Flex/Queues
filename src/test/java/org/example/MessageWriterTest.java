package org.example;

import org.apache.commons.csv.CSVPrinter;
import org.example.user.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;


class MessageWriterTest {
    private MessageWriter messageWriter;
    @Mock
    private CSVPrinter correctCsvWriter;
    @Mock
    private CSVPrinter errorsCsvWriter;
    @Captor
    private ArgumentCaptor<String> captorString;
    @Captor
    private ArgumentCaptor<Integer> captorInt;

    @BeforeEach
    void setup() throws IllegalAccessException, IOException {
        initMocks(this);
        messageWriter = new MessageWriter();
        doNothing().when(correctCsvWriter).printRecord(any(String.class), any(Integer.class));
        doNothing().when(errorsCsvWriter).printRecord(any(String.class), any(Integer.class));
        Field[] fields = messageWriter.getClass().getDeclaredFields();
        fields[0].setAccessible(true);
        fields[0].set(messageWriter, correctCsvWriter);
        fields[1].setAccessible(true);
        fields[1].set(messageWriter, errorsCsvWriter);
    }
    @ParameterizedTest(name = "name = {0}, count = {1}, eddr = {2}")
    @CsvFileSource(resources = "/consumerMessagesCorrect.csv")
    void messageIsCorrectTest_CorrectMessages(String name, int count, String eddr) {
        messageWriter = new MessageWriter();
        User user = new User();
        user.setName(name);
        user.setEddr(eddr);
        user.setCount(count);

        assertTrue(messageWriter.messageIsCorrect(user));
    }
    @ParameterizedTest(name = "name = {0}, count = {1}, eddr = {2}")
    @CsvFileSource(resources = "/consumerMessagesIncorrect.csv")
    void messageIsCorrectTest_IncorrectMessages(String name, int count, String eddr) {
        messageWriter = new MessageWriter();
        User user = new User();
        user.setName(name);
        user.setEddr(eddr);
        user.setCount(count);

        assertFalse(messageWriter.messageIsCorrect(user));
    }

    @ParameterizedTest(name = "name = {0}, count = {1}, eddr = {2}")
    @CsvFileSource(resources = "/consumerMessagesIncorrect.csv")
    void writeCorrectMessage_CorrectMessages(String name, int count, String eddr) throws IOException {
        User user = new User();
        user.setName(name);
        user.setEddr(eddr);
        user.setCount(count);

        messageWriter.writeCorrectMessage(user);
        verify(correctCsvWriter, times(1)).printRecord(captorString.capture(), captorInt.capture());
        assertAll(
                () -> assertEquals(name, captorString.getValue()),
                () -> assertEquals(count, captorInt.getValue())
        );
    }
    @ParameterizedTest(name = "name = {0}, count = {1}, eddr = {2}, errors = {3}")
    @CsvFileSource(resources = "/consumerMessagesIncorrect.csv")
    void writeCorrectMessage_IncorrectMessages(String name, int count, String eddr, String errors) throws IOException {
        User user = new User();
        user.setName(name);
        user.setEddr(eddr);
        user.setCount(count);

        messageWriter.writeIncorrectMessage(user);
        verify(errorsCsvWriter, times(1)).printRecord(captorString.capture(), captorInt.capture()
                                                                                    ,captorString.capture());
        List<String> list = captorString.getAllValues();
        assertAll(
                () -> assertEquals(name, list.get(0)),
                () -> assertEquals(errors, list.get(1)),
                () -> assertEquals(count, captorInt.getValue())
        );
    }
}