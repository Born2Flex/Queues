package org.example.user;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class UserGeneratorTest {
    private UserGenerator userGenerator;

    @BeforeEach
    void setup() {
        userGenerator = new UserGenerator();
    }
    @Test
    void generateUserTest_ProductivityTest() {
        long startTime = System.currentTimeMillis();
        int k = 0;

        while (System.currentTimeMillis() - startTime < 1000) {
            userGenerator.generateUser();
            k++;
        }

        assert k >= 50_000;
    }
}