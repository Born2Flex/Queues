package org.example;

import org.example.user.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.jms.*;

import static org.mockito.Mockito.*;

class ConsumerTest {
    private Consumer consumer;
    private Session session;
    private MessageConsumer messageConsumer;
    private MessageWriter messageWriter;
    @BeforeEach
    void setup() throws JMSException {
        messageConsumer = mock(MessageConsumer.class);
        messageWriter = mock(MessageWriter.class);
        session = mock(Session.class);
        Queue destination = mock(Queue.class);

        when(session.createQueue("test")).thenReturn(destination);
        when(session.createConsumer(destination)).thenReturn(messageConsumer);
    }

    @Test
    void processAndWriteMessageTest_CatchedPoisonPill() throws JMSException {
        consumer = new Consumer(session, messageWriter,"test");
        TextMessage message = mock(TextMessage.class);
        when(message.getText()).thenReturn("POISON PILL: STOP READING");
        when(messageConsumer.receive()).thenReturn(message);
        consumer.receiveMessages();
        verify(messageWriter, never()).writeErrorMessage(anyString());
    }

    @Test
    void processAndWriteMessageTest_ReceivingMessages() throws JMSException {
        consumer = new Consumer(session, messageWriter,"test");
        TextMessage message = mock(TextMessage.class);
        when(message.getText()).thenReturn("{\"name\":\"Bstakart\",\"eddr\":\"2005051601671\",\"count\":59,\"creationTime\":\"2023-09-27T18:44:14.929194700\"}")
                                .thenReturn("{\"name\":\"dsvvt\",\"eddr\":\"2325051601671\",\"count\":59,\"creationTime\":\"2023-09-27T18:44:14.929194700\"}")
                                .thenReturn("{\"name\":\"dsvvt\",\"eddr\":\"2325051601671\",\"count\":59,\"creationTime\":\"2023-09-27T18:44:14.929194700\"}")
                                .thenReturn("POISON PILL: STOP READING");
        when(messageConsumer.receive()).thenReturn(message);
        consumer.receiveMessages();
        verify(messageWriter, times(3)).messageIsCorrect(any(User.class));
    }
}